#!/bin/bash
#
# Palimpsest multiboot USB creation.
# Copyright (C) 2020  Guillermo Chamorro <palimpsest at protonmail.com>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# Palimpsest version 1.0, Copyright (C) 2020 Guillermo Chamorro.
# Palimpsest comes with ABSOLUTELY NO WARRANTY.
#
####################################
# Script description: scan and shows available isos
####################################

####################################
# Scans for installed isos in the pendrive
####################################
scan_installed_isos() {
  local _isos_installed=()

  isos_installed_file="/tmp/isos_installed"
  isos_persistence_file="/tmp/isos_persistence_installed"
  isos_to_show_file="/tmp/isos_to_show"

  echo -n > "$isos_installed_file"
  echo -n > "$isos_to_show_file"
  echo -n > "$isos_persistence_file"
  echo -n > "$installed_persistence_file"

  message "${lang["scan_installed_isos_scan"]}"

  readarray -d '' _isos_installed < <(
    find "$pendrive_isos_dir" \
      -type f \
      -name "*.iso" \
      -print0
  )

  readarray -d '' _isos_persistence_scan < <(
    find "$pendrive_isos_dir" \
      -type f \
      \( -name "writable" -o -name "casper-rw" -o -name "persistence" \) \
      -print0
  )

  for iso in "${_isos_installed[@]}"; do
    echo "$iso" >> "$isos_to_show_file"
    basename "$iso" >> "$isos_installed_file"
  done

  for persistence in "${_isos_persistence_scan[@]}"; do
    basename "$(dirname "$persistence")" >> "$installed_persistence_file"
  done

  grep -f "$supported_persistence_file" "$isos_to_show_file" > "$isos_persistence_file"
}

####################################
# Scans for isos in the disks
####################################
scan_disks_isos() {
  local _isos_disks_scan=()
  local _isos_disks_found_file="/tmp/isos_disks_found.txt"
  local _isos_disks_supported_file="/tmp/isos_disks_supported.txt"

  . disks

  # if the cache file is empty try to populate it with the isos found in disks
  if [[ ! -s "$cache_disks_isos_file" ]]; then
    for disk in "${disks_mount_folders[@]}"; do
      path="$disk"

      [[ -f "$disk/bootmgr" ]] && path="$disk/Users"
      [[ -d "$disk/home" ]] && path="$disk/home"

      readarray -d '' _isos_disks_scan < <(
        find "$path" \
          -type f \
          -name "*.iso" \
          -print0
      )
      if [[ ! -z "${_isos_disks_scan}" ]]; then
        for iso in "${_isos_disks_scan[@]}"; do
          echo "$iso" >> "${cache_disks_isos_file}"
        done
      fi
    done
  fi

  # if cache file is still empty then no iso was found
  if [[ ! -s "${cache_disks_isos_file}" ]]; then
    whiptail \
      --backtitle "$backtitle" \
      --title "${lang["show_and_select_isos_no_isos_title"]}" \
      --msgbox "${lang["show_and_select_isos_no_isos_msg"]}" \
      8 30

    start_menu
  else
      sed 's/:distro\|:tools//' "$supported_isos_file" > "$tmp_file"

      grep -f "$tmp_file" "$cache_disks_isos_file" > "$_isos_disks_supported_file"
      grep -vf "$isos_installed_file" "$_isos_disks_supported_file" > "$isos_to_show_file"
  fi
}

####################################
# Shows a list of isos available isos to install or uninstall
####################################
show_and_select_isos() {
  local _isos_to_show_choices=()

  if [[ ! -s "$isos_to_show_file" ]]; then
    whiptail \
      --backtitle "$backtitle" \
      --title "${lang["show_and_select_isos_no_isos_title"]}" \
      --msgbox "${lang["$no_isos_found_msg"]}" \
      9 70

      start_menu
  fi

  # sort the isos to show
   sed 's;^\(.*/\)\(.*\)$;\2 \1\2;' "$isos_to_show_file" \
     | sort \
     | awk '{ print $2 }' > "$tmp_file"

  cat "$tmp_file" > "$isos_to_show_file"

  while IFS= read -r iso_to_show; do
    _isos_to_show_choices+=("$iso_to_show" "$(basename "$iso_to_show")")
  done < "$isos_to_show_file"

  whiptail \
    --backtitle "$backtitle" \
    --title "${lang["show_and_select_isos_title"]}" \
    --notags \
    --menu "${lang["show_and_select_isos_menu_msg"]}" \
    30 80 15 \
    "${_isos_to_show_choices[@]}" \
    2> "${tmp_file}"

  local response=$?

  case "$response" in
    0) ;;
    *) . systems_tasks;;
  esac

  read -r -a selected_iso < "${tmp_file}"

  selected_iso_name=$(basename "$selected_iso")
  selected_iso_name="${selected_iso_name%%.iso}"
}
