#!/bin/bash
#
# Palimpsest multiboot USB creation.
# Copyright (C) 2020  Guillermo Chamorro <palimpsest at protonmail.com>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# Palimpsest version 1.0, Copyright (C) 2020 Guillermo Chamorro.
# Palimpsest comes with ABSOLUTELY NO WARRANTY.
#
####################################
# Script description: creates the grub config file for each system
####################################

####################################
# Creates a grub entry
####################################
create_grub_entry() {
  message "${lang["create_grub_entry_msg"]}"

  if [[ ! -z $(echo "/$selected_iso:distro" | grep -f "$supported_isos_file") ]]; then
cat << EOF >> "$pendrive_palimpsest_mount_dir/boot/grub/distros.cfg"
menuentry '${selected_iso_name}' {insmod all_video;configfile /boot/grub/${selected_iso_name}.cfg}
EOF
  elif [[ ! -z $(echo "/$selected_iso:tools" | grep -f "$supported_isos_file") ]]; then
cat << EOF >> "$pendrive_palimpsest_mount_dir/boot/grub/tools.cfg"
menuentry '${selected_iso_name}' {insmod all_video;configfile /boot/grub/${selected_iso_name}.cfg}
EOF
  fi
}

####################################
# Copies the grub file from the ISO, or creates an empty one
####################################
create_grub_file() {
  mount_iso "$selected_iso"

  message "${lang["copy_grub_file_from_iso_copying_grub_file"]}"

  if [[ -f "$iso_mount_dir/boot/grub/grub.cfg" ]]; then
    cp "$iso_mount_dir/boot/grub/grub.cfg" "$pendrive_palimpsest_mount_dir/boot/grub/${selected_iso_name}.cfg"
  elif [[ -f "$iso_mount_dir/boot/grub/loopback.cfg" ]]; then
    cp "$iso_mount_dir/boot/grub/loopback.cfg" "$pendrive_palimpsest_mount_dir/boot/grub/${selected_iso_name}.cfg"
  elif [[ -f "$iso_mount_dir/EFI/debian/grub/grub.cfg" ]]; then
    cp "$iso_mount_dir/EFI/debian/grub/grub.cfg" "$pendrive_palimpsest_mount_dir/boot/grub/${selected_iso_name}.cfg"
  elif [[ -f "$iso_mount_dir/EFI/BOOT/grub.cfg" ]]; then
    cp "$iso_mount_dir/EFI/BOOT/grub.cfg" "$pendrive_palimpsest_mount_dir/boot/grub/${selected_iso_name}.cfg"
  else
    echo -n > "$pendrive_palimpsest_mount_dir/boot/grub/${selected_iso_name}.cfg"
  fi

  unmount_iso
}

####################################
# Edits the ISO grub file
####################################
edit_iso_grub_file() {
  sed -i "1iset root='(hd0,1)'" \
    "${pendrive_palimpsest_mount_dir}/boot/grub/${selected_iso_name}.cfg"
  sed -i "2iset iso_path=\/isos\/${selected_iso_name}\/${selected_iso_name}.iso" \
    "$pendrive_palimpsest_mount_dir/boot/grub/${selected_iso_name}.cfg"
  sed -i '3iloopback loop $iso_path' \
    "${pendrive_palimpsest_mount_dir}/boot/grub/${selected_iso_name}.cfg"

  # for most debian and ubuntu systems
  sed -i "s#linux[[:blank:]]*/#linux (loop)/#" \
    "${pendrive_palimpsest_mount_dir}/boot/grub/${selected_iso_name}.cfg"
  sed -i "s#initrd[[:blank:]]*/#initrd (loop)/#" \
    "${pendrive_palimpsest_mount_dir}/boot/grub/${selected_iso_name}.cfg"
  sed -i "s#linux (loop)\([^ ]*\)#linux (loop)\1 findiso=\$iso_path iso-scan/filename=\$iso_path #" \
    "${pendrive_palimpsest_mount_dir}/boot/grub/${selected_iso_name}.cfg"
  sed -i "s#/boot/grub#(loop)/boot/grub#" \
    "${pendrive_palimpsest_mount_dir}/boot/grub/${selected_iso_name}.cfg"
  sed -i "s# /grub# (loop)/grub#" \
    "${pendrive_palimpsest_mount_dir}/boot/grub/${selected_iso_name}.cfg"

  # for clonezilla new versions
  sed -i "s#\$linux_cmd[[:blank:]]*/#\$linux_cmd (loop)/#" \
    "${pendrive_palimpsest_mount_dir}/boot/grub/${selected_iso_name}.cfg"
  sed -i "s#\$initrd_cmd[[:blank:]]*/#\$initrd_cmd (loop)/#" \
    "${pendrive_palimpsest_mount_dir}/boot/grub/${selected_iso_name}.cfg"
  sed -i "s#\$linux_cmd (loop)\([^ ]*\)#\$linux_cmd (loop)\1 findiso=\$iso_path iso-scan/filename=\$iso_path #" \
    "${pendrive_palimpsest_mount_dir}/boot/grub/${selected_iso_name}.cfg"
}

####################################
# Main function to execute the script
####################################
main() {
  create_grub_entry

  create_grub_file

  edit_iso_grub_file

  . grub_configuration_specific
}

main
